queue()
    .defer(d3.json, "/metadecidim/data")
    .await(makeGraphs);

function makeGraphs(error, projectsJson, statesJson) {
	
	//Clean projectsJson data
	var donorschooseProjects = projectsJson;

	//Create a Crossfilter instance
	var ndx = crossfilter(donorschooseProjects);

	//Define Dimensions
	var categoryDim = ndx.dimension(function(d) { return d["category__name"]; });
	var statusDim = ndx.dimension(function(d) { return d["status"]; });
	var districtDim = ndx.dimension(function(d) { return d["district__name"]; });
    var rejectedDim = ndx.dimension(function(d){ return d["rejected_category"]})
    var sourceDim = ndx.dimension(function(d){ return d["source"]})


	//Calculate metrics
	var numProjectsByCategory = categoryDim.group();
	var numProjectsByStatus = statusDim.group();
    var numProjectsByDistrict = districtDim.group();
    var numProjectsByRejected = rejectedDim.group()
    var numProjectsBySource = sourceDim.group()

	var all = ndx.groupAll();

    //Charts
	var categoriesChart = dc.pieChart("#categories-pie-chart");
	var statusChart = dc.pieChart("#status-pie-chart");
    var districtChart = dc.rowChart('#district-row-chart')
	var numberProposalsND = dc.numberDisplay("#number-proposals-nd");
    var rejectedChart = dc.pieChart('#rejected-chart')
    var sourceChart = dc.pieChart('#source-chart')

	numberProposalsND
		.formatNumber(d3.format("d"))
		.valueAccessor(function(d){return d; })
		.group(all);

	categoriesChart
        .dimension(categoryDim)
        .group(numProjectsByCategory)

	statusChart
        .dimension(statusDim)
        .group(numProjectsByStatus)

    districtChart
        .width(600)
        .height(460)
        .dimension(districtDim)
        .group(numProjectsByDistrict)
        .xAxis().ticks(8)

    rejectedChart
        .dimension(rejectedDim)
        .group(numProjectsByRejected)

    sourceChart
        .dimension(sourceDim)
        .group(numProjectsBySource)

    dc.renderAll();

};
