def modifyData(dic, key):
    if key == 'district__name':
        if not dic[key]:
            dic[key] = 'Barcelona'
    if key == 'status':
        value = dic[key]
        if value == '0':
           dic[key] = 'rebutjat'
        elif value == '1':
            dic[key] = 'acceptat'
    return dic[key]

def filterData(data, filter_fields):
    new_data = []
    for d in data:
        new_line = {}
        for key in filter_fields:
            new_line[key] = modifyData(d,key)
        new_data.append(new_line)
    return new_data
