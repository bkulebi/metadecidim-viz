from flask import Flask
from flask import render_template
import json
#from bson import json_util
#from bson.json_util import dumps

import data_ops
import io_tools

app = Flask(__name__)

filter_fields = ["category__name", "status", "district__name", "rejected_category", "source"]

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/metadecidim/data")
def donorschoose_projects():
    keys, data = io_tools.getCsvReader('proposal_enriched_clean02.csv',',')
    filtered_data = data_ops.filterData(data, filter_fields)
    json_projects = json.dumps(filtered_data)
    return json_projects

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=True)
